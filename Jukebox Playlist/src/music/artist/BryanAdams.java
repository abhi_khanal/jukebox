package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BryanAdams {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BryanAdams() {
    }
    
    public ArrayList<Song> getBryanAdamsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Here I am", "Bryan Adams");    				//Create a song
         Song track2 = new Song("Summer of 69", "Bryan Adams");         		//Create another song
         Song track3 = new Song("Everything I do", "Bryan Adams");         		//Create a third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Bryan Adams
         this.albumTracks.add(track2);                                          //Add the second song to song list for Bryan Adams 
         this.albumTracks.add(track3);                                          //Add the third song to song list for Bryan Adams 
         return albumTracks;                                                    //Return the songs for Bryan Adams in the form of an ArrayList
    }
}
