package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Scorpions {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Scorpions() {
    }
    
    public ArrayList<Song> getScorpionsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Rock you like a hurricane", "Scorpions");      //Create a song
         Song track2 = new Song("You and I", "Scropions");         				//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Scorpions
         this.albumTracks.add(track2);                                          //Add the second song to song list for Scorpions 
         return albumTracks;                                                    //Return the songs for Scorpions in the form of an ArrayList
    }
}
