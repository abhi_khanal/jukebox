package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class TravisOwens_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> alexiMurdochTracks = new ArrayList<Song>();
	AlexiMurdoch alexiMurdochBand = new AlexiMurdoch();
	
	alexiMurdochTracks = alexiMurdochBand.getAlexiMurdochSongs();
	
	playlist.add(alexiMurdochTracks.get(0));
	playlist.add(alexiMurdochTracks.get(1));
	playlist.add(alexiMurdochTracks.get(2));
	
	
    Hozier hozierBand = new Hozier();
	ArrayList<Song> hozierTracks = new ArrayList<Song>();
	hozierTracks = hozierBand.getHozierSongs();
	
	playlist.add(hozierTracks.get(0));
	playlist.add(hozierTracks.get(1));
	
    return playlist;
	}
}
