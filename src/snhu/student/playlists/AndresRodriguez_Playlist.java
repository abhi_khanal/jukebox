package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class AndresRodriguez_Playlist {
	
    //Returns a LinkedList of songs added to Andres Rodriguez playlist
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	TheBeatles theBeatlesBand = new TheBeatles();
	ArrayList<Song> beatlesTracks = new ArrayList<Song>();
    beatlesTracks = theBeatlesBand.getBeatlesSongs();
	
    playlist.add(beatlesTracks.get(0));
	playlist.add(beatlesTracks.get(1));
	
    ImagineDragons imagineDragonsBand = new ImagineDragons();
	ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
	
	playlist.add(imagineDragonsTracks.get(0));
	playlist.add(imagineDragonsTracks.get(1));
	playlist.add(imagineDragonsTracks.get(2));
	
	JimmyEatWorld jimmyEatWorld = new JimmyEatWorld();
	ArrayList<Song> jimmyEatWorldTracks = new ArrayList<Song>();
	jimmyEatWorldTracks = jimmyEatWorld.getJimmyEatWorldSongs();
	
	playlist.add(jimmyEatWorldTracks.get(0));
	playlist.add(jimmyEatWorldTracks.get(1));
	playlist.add(jimmyEatWorldTracks.get(2));
	
	EltonJohn eltonJohn = new EltonJohn();
	ArrayList<Song> eltonJohnTracks = new ArrayList<Song>();
	eltonJohnTracks = eltonJohn.getEltonJohnSongs();
	
	playlist.add(eltonJohnTracks.get(0));
	playlist.add(eltonJohnTracks.get(1));
	playlist.add(eltonJohnTracks.get(2));
	
	MichaelJackson michaelJackson = new MichaelJackson();
	ArrayList<Song> michaelJacksonTracks = new ArrayList<Song>();
	michaelJacksonTracks = michaelJackson.getMichaelJacksonSongs();
	
	playlist.add(jimmyEatWorldTracks.get(0));
	playlist.add(jimmyEatWorldTracks.get(1));
	playlist.add(jimmyEatWorldTracks.get(2));
	
    return playlist;
	}
}
