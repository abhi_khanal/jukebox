package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class MichaelJackson {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public MichaelJackson() {
    }
    
    public ArrayList<Song> getMichaelJacksonSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Billie Jean", "Michael Jackson");         				//Create a song
         Song track2 = new Song("P.Y.T.", "Michael Jackson");        //Create another song
         Song track3 = new Song("Man in the Mirror", "Michael Jackson");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Michael Jackson in the form of an ArrayList
    }
}