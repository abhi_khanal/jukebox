package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class GreenDay {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GreenDay() {
    }
    
    public ArrayList<Song> getGreenDaySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("American Idiot", "Green Day");         				//Create a song
         Song track2 = new Song("Wake Me Up When September Ends", "Green Day");        //Create another song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         return albumTracks;                                            //Return the songs for Green Day in the form of an ArrayList
    }
}